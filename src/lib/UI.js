import { EventDispatcher, Object3D } from "three";
import tools from "tools";

export default class UI extends EventDispatcher {

    constructor(mainCamera) {
        super();

        this._parseObj = this._parseObj.bind(this);

        this._container = new Object3D();
        this._container.renderOrder = 999;
        mainCamera.add(this._container);

    }





    _parseObj(displayObject) {

        if (displayObject.isMesh || displayObject.type == "Sprite") {
            displayObject.material.transparent = true;
            displayObject.material.depthTest = false;
            displayObject.renderOrder = 999;
        } else {

            for (let i = 0; i < displayObject.children.length; i++) {
                const obj = displayObject.children[i];
                if (obj.isMesh || obj.type == "Sprite") {
                    obj.material.transparent = true;
                    obj.material.depthTest = false;
                    obj.renderOrder = 999;

                } else if (obj.children && obj.children.length > 0) {
                    this._parseObj(obj);
                }
            }

        }

    }


    add(obj) {

        this._parseObj(obj);
        this._container.add(obj);

    }


    remove(obj) {

        this._container.remove(obj);

    }

    set z(val) {
        this._container.position.z = val;
    }


    dispose() {

        tools.clean(this._container);

    }



}