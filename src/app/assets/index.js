
import { RepeatWrapping, EventDispatcher, DoubleSide, MeshToonMaterial, PMREMGenerator, FontLoader, TextureLoader, MeshStandardMaterial, MeshLambertMaterial } from 'three';
import { GLTFLoader } from 'lib/GLTFLoader';
import config from "config";
import tools from "tools";
import TexturesLoader from "lib/TexturesLoader";
import { Howl } from "howler";
import assetsRaw from './assetsRaw';
import { DEG2RAD } from 'three/src/math/MathUtils';


class Assets extends EventDispatcher {
    constructor() {

        super();

        this.totalItems = 0;
        this.loadedItems = 0;
        this.progress = 0;

        this.parseGltfData = this.parseGltfData.bind(this);
        this.getAsset = this.getAsset.bind(this);
        this.initMaterials = this.initMaterials.bind(this);
        this.progressUpdate = this.progressUpdate.bind(this);


        this.textures = {
            envMap: config.assets + "/img/envmap.jpg",
            skyMap: config.assets + "/img/equirect.jpg",
            piege: config.assets + "/img/piege.jpg",
            blood0: config.assets + "/img/blood0.png",
            blood1: config.assets + "/img/blood1.png",
            bloodImpact0: config.assets + "/img/bloodImpact0.png",
            bloodImpact1: config.assets + "/img/bloodImpact1.png",
            bloodImpact2: config.assets + "/img/bloodImpact2.png",
            bloodImpact3: config.assets + "/img/bloodImpact3.png",
            damier: config.assets + "/img/damier.png",
            waternomal: config.assets + "/img/waternomal.jpg",
            motif: config.assets + "/img/motif.png",
            relief: config.assets + "/img/relief.jpg",
            gradienwall: config.assets + "/img/gradienwall.png",
            cloud: config.assets + "/img/cloud.png",
            lava: config.assets + "/img/lava.jpg",
            you: config.assets + "/img/you.png",
            faceMap: config.assets + "/img/faceMap.png",
            start: config.assets + "/img/start.png"
        };

        for (const key in this.textures) {
            this.totalItems++;
        }


        this.gltf = {
            level: config.assets + "/gltf/level.glb",
            doodle: config.assets + "/gltf/doodle.glb"
        };

        for (const key in this.gltf) {
            this.totalItems++;
        }

        //audio item
        this.totalItems++;


        this.loadScreen = new TextureLoader().load(assetsRaw.loadscreen);
        this.title = new TextureLoader().load(assetsRaw.title);

        this.assets = {};
        this.gltfArray = [];
        this.gltfLoaded = 0;


        this.texturesLoader = new TexturesLoader();

        this.texturesLoader.addEventListener("onProgress", event => {
            this.progressUpdate();
        });

        this.texturesLoader.addEventListener("onLoad", event => {

            for (const key in event.textures) {
                this.textures[key] = event.textures[key];
            }

            console.log("Textures ready")

            this.fx = new Howl({
                src: [config.assets + "/audio/fx.mp3"],
                sprite: {
                    dong: [0, 235.1020408163265],
                    put: [2000, 208.97959183673453],
                    victory: [4000, 3578.775510204082],
                    fail: [9000, 288.52607709750623]
                },
                onload: () => {
                    console.log("Audio ready");

                    this.progressUpdate();
                    this.nextAssetsLoading();

                }
            });



        });



    }




    initMaterials() {


        this.tobogganMat = new MeshStandardMaterial({
            color: 0xe9913a,
            metalness: 0.05,
            roughness: 0.4,
            envMap: this.textures.envMap,
            side: DoubleSide
        });
        this.tobogganMat.userData.outlineParameters = {
            visible: false
        };



        this.piegeMat = new MeshStandardMaterial({
            color: 0xffeeee,
            metalness: 0.1,
            roughness: 0.5,
            map: this.textures.piege,
            envMap: this.textures.envMap,
            envMapIntensity: .6,
            side: DoubleSide
        });

        this.startMat = new MeshStandardMaterial({
            color: 0xffeeee,
            metalness: 0.1,
            roughness: 0.5,
            map: this.textures.start,
            envMap: this.textures.envMap,
            envMapIntensity: .6,
            side: DoubleSide
        });


        this.reliefMat = new MeshStandardMaterial({
            color: 0xffffff,
            metalness: 0.1,
            roughness: 0.6,
            map: this.textures.relief,
            envMap: this.textures.envMap,
            side: DoubleSide
        });

        this.reliefMat.userData.outlineParameters = {
            visible: false
        };


        this.boxMat = new MeshStandardMaterial({
            color: 0xffffff,
            metalness: .1,
            roughness: .8,
            map: this.textures.motif,
            envMap: this.textures.envMap,
            envMapIntensity: .4,
            side: DoubleSide
        });

        this.boxMat.userData.outlineParameters = {
            visible: false
        };


        this.endMat = new MeshStandardMaterial({
            color: 0xffffff,
            metalness: 0.0,
            roughness: 0.4,
            map: this.textures.damier,
            envMap: this.textures.envMap,
            side: DoubleSide
        });

        this.endMat.userData.outlineParameters = {
            visible: false
        };





    }


    initAssets() {


        let pmremGenerator = new PMREMGenerator(config.renderer);
        pmremGenerator.compileEquirectangularShader();
        this.textures.envMap = pmremGenerator.fromEquirectangular(this.textures.envMap).texture;
        this.textures.skyMap = pmremGenerator.fromEquirectangular(this.textures.skyMap).texture;

        this.textures.blood0.anisotropy = 12;
        this.textures.blood1.anisotropy = 12;
        this.textures.bloodImpact0.anisotropy = 12;
        this.textures.bloodImpact1.anisotropy = 12;
        this.textures.bloodImpact2.anisotropy = 12;
        this.textures.bloodImpact3.anisotropy = 12;
        this.textures.faceMap.anisotropy = 12;
        this.textures.piege.anisotropy = 12;
        this.textures.start.anisotropy = 12;
        this.textures.damier.wrapS = this.textures.damier.wrapT = RepeatWrapping;
        this.textures.damier.anisotropy = 12;
        this.textures.damier.flipY = false;
        this.textures.waternomal.wrapS = this.textures.waternomal.wrapT = RepeatWrapping;
        this.textures.motif.wrapS = this.textures.motif.wrapT = RepeatWrapping;
        this.textures.motif.anisotropy = 12;
        this.textures.relief.wrapS = this.textures.relief.wrapT = RepeatWrapping;
        this.textures.relief.anisotropy = 12;
        this.textures.gradienwall.wrapS = this.textures.gradienwall.wrapT = RepeatWrapping;
        this.textures.cloud.wrapS = this.textures.cloud.wrapT = RepeatWrapping;
        this.textures.lava.wrapS = this.textures.lava.wrapT = RepeatWrapping;
        this.textures.you.anisotropy = 12;

    }



    load() {

        //json file
        this.totalItems++;

        tools.load(config.assets + "/json/level.json", event => {

            config.isDebug = event.debug;
            config.cannon_debug = event.cannon_debug;
            config.infos = event.infos;
            config.levelID = parseInt(event.levelID);
            config.playerSpeedTime = parseFloat(event.playerSpeedTime);
            this.levelsData = event.levels;

            this.progressUpdate();
            this.texturesLoader.load(this.textures);

        });



    }





    nextAssetsLoading() {

        this.initAssets();

        this.initMaterials();


        this.fontLoader = new FontLoader();
        this.fontLoader.load(config.assets + "/fonts/impactregular.json", font => {
            this.font3d = font;
            console.log("Font3d ready");


            for (const key in this.gltf) {
                this.gltfArray.push({ path: this.gltf[key], id: key })
            }


            //Next loading
            if (this.gltfArray.length > 0) {

                for (let i = 0; i < this.gltfArray.length; i++) {

                    let gltfObj = new GLTFLoader();
                    gltfObj.resourcePath = this.gltfArray[i].id;
                    gltfObj.load(this.gltfArray[i].path, gltf => {

                        this.progressUpdate();

                        if (gltf.animations.length <= 0) {

                            this.parseGltfData(gltf);

                        } else {

                            this.gltf[gltf.parser.options.path] = gltf;

                            this.gltfLoaded++;

                            if (this.gltfLoaded >= this.gltfArray.length) {

                                console.log("GLTF ready");
                                this.dispatchEvent({ type: "ready" });

                            }

                        }

                    });

                }

            } else {

                this.dispatchEvent({ type: "ready" });

            }


        });

    }








    parseGltfData(gltf) {


        this.gltfLoaded++;

        let _scene = gltf.scene;
        let _object;

        this.assets.level = _scene;


        _scene.children.map((object) => {

            _object = tools.parseObject(object);

            if (_object) {

                console.log(_object.name);

                _object.visible = false;

                switch (_object.name) {

                    case "box0":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.box0 = _object;
                        break;

                    case "box1":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.box1 = _object;
                        break;

                    case "box2":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.box2 = _object;
                        break;


                    case "tube":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.tube = _object;
                        break;

                    case "tube2":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.tube2 = _object;
                        break;


                    case "piege1":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.piege1 = _object;
                        break;


                    case "piege2":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.piege2 = _object;

                        break;


                    case "piege3":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;


                        break;


                    case "lame":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.lame = _object;
                        break;


                    case "box3":

                        _object.material = this.endMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;

                    case "box4":

                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;

                    case "box5":

                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;

                    case "box6":

                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;

                    case "piege3a":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;


                    case "piege3b":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;

                    case "arrow":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        break;

                    case "piege4":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.piege4 = _object;
                        break;


                    case "lame2":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.lame2 = _object;
                        break;


                    case "relief":
                        _object.material = this.reliefMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        _object.visible = true;
                        break;


                    case "segment":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        _object.rotation.y = DEG2RAD * 180;
                        this.assets.segment = _object;
                        break;


                    case "left":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.left = _object;
                        break;

                    case "right":
                        _object.material = this.boxMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.right = _object;
                        break;


                    case "start":
                        _object.material = this.piegeMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.start = _object;
                        break;


                    case "end":
                        _object.material = this.startMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.assets.end = _object;
                        break;


                }
            }


        });



        if (this.gltfLoaded >= this.gltfArray.length) {

            this.dispatchEvent({ type: "ready" });

        }


    }



    /**
     * Not animated GLTF
     * @param {String} name 
     * @returns Mesh
     */

    getAsset(name) {

        return this.assets[name].dontclone ? this.assets[name] : this.assets[name].clone();

    }




    progressUpdate() {

        this.loadedItems++;
        this.progress = this.loadedItems / this.totalItems;
        this.dispatchEvent({ type: "onProgress", progress: this.progress });

    }




}

export default new Assets();