import assets from "app/assets";
import SolidObject from "app/modules/common/solidObject";
import { BoxBufferGeometry, BoxGeometry, Color, Mesh, Object3D, Quaternion, Vector3 } from "three";
import tools from "tools";
import * as CANNON from 'cannon-es';
import { DEG2RAD } from "three/src/math/MathUtils";
import Blade from "./blade";
import gsap from "gsap/all";
import Blade2 from "./blade2";

export default class Chopper2 extends Object3D {


    constructor(name, nbr) {
        super();

        this.build = this.build.bind(this);
        this.startEngine = this.startEngine.bind(this);

        this.name = name;
        this._nbr = nbr;
        this._type = "chopper2";
        this._delay = 1;
        this._state = "close";
        this._callBacks = [];
        this._dummyRotation = new Object3D();
        this._lames = [];


        this.build();

    }



    build() {

        let lame;
        let pos = 0;

        for (let i = 0; i < this._nbr; i++) {

            lame = new Blade2();
            lame.rotation.y = DEG2RAD * -90;
            lame.position.z = pos;
            pos -= .22;
            this.add(lame);
            this._lames.push(lame);

        }



        this.scale.set(1.2, 1.2, 1.2);




    }


    startEngine(d) {


        gsap.delayedCall(d, () => {

            this.interval = setInterval(() => {

                for (let i = 0; i < this._lames.length; i++) {
                    this._lames[i].start(.25, i * 0.1);
                }

                this._state = "open";

                gsap.delayedCall(1, () => {
                    this._state = "close";
                });

            }, 3000);


        });



    }





    dispatch(event) {

        console.log("debug ", event)

    }



    start(delay) {
        this.startEngine(delay);
    }



    setDelay(val) {
        this._delay = val;
    }



    get state() {
        return this._state;
    }



    update() {

        for (let i = 0; i < this._lames.length; i++) {
            this._lames[i].update();
        }

    }


    dispose() {

        this.interval && clearInterval(this.interval);
        tools.clean(this);
    }






}