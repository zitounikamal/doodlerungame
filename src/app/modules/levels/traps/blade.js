import assets from "app/assets";
import config from "config";
import gsap from "gsap/all";
import { Mesh, MeshBasicMaterial, Object3D, PlaneBufferGeometry } from "three";
import { DEG2RAD } from "three/src/math/MathUtils";

export default class Blade extends Object3D {

    constructor() {

        super();

        this._lame = assets.getAsset("lame");


        this.build();

    }


    build() {

        this._lame.visible = true;
        this._lame.position.x = 0;
        this._lame.position.z = 0;
        this.add(this._lame);

        this.matBlood0 = new MeshBasicMaterial({ color: config.player0Color });
        this.matBlood0.transparent = true;
        this.matBlood0.map = assets.textures.blood0;

        this.matBlood1 = new MeshBasicMaterial({ color: config.player1Color });
        this.matBlood1.transparent = true;
        this.matBlood1.map = assets.textures.blood0;

        this.matBlood2 = new MeshBasicMaterial({ color: config.player2Color });
        this.matBlood2.transparent = true;
        this.matBlood2.map = assets.textures.blood0;





        this.blood0 = new Mesh(new PlaneBufferGeometry(.2, .2), this.matBlood0);
        this.blood0.rotation.x = DEG2RAD * 90;
        this.blood0.rotation.y = DEG2RAD * 180;

        this.blood0.position.y = 0.187;
        this.blood0.position.x = -.32;
        this.blood0.position.z = -.05;

        this.blood0.scale.y = .9;
        this.blood0.scale.x = 1.2;

        this.blood0.visible = false;

        this.add(this.blood0);



        this.blood1 = new Mesh(new PlaneBufferGeometry(.2, .2), this.matBlood1);
        this.blood1.rotation.x = DEG2RAD * 90;
        this.blood1.rotation.y = DEG2RAD * 180;

        this.blood1.position.y = 0.187;
        this.blood1.position.x = -.57;
        this.blood1.position.z = -.05;

        this.blood1.scale.y = .9;
        this.blood1.scale.x = 1.2;

        this.blood1.visible = false;

        this.add(this.blood1);



        this.blood2 = new Mesh(new PlaneBufferGeometry(.2, .2), this.matBlood2);
        this.blood2.rotation.x = DEG2RAD * 90;
        this.blood2.rotation.y = DEG2RAD * 180;

        this.blood2.position.y = 0.187;
        this.blood2.position.x = -.83;
        this.blood2.position.z = -.05;

        this.blood2.scale.y = .9;
        this.blood2.scale.x = 1.2;

        this.blood2.visible = false;

        this.add(this.blood2);

    }









    showBlood(id) {

        switch (id) {

            case 0:
                this.blood0.visible = true;
                this.matBlood0.opacity = 1;
                gsap.to(this.matBlood0, {
                    duration: 1,
                    delay: 3.5, opacity: 0, onComplete: () => {
                        this.blood0.visible = false;
                    }
                });

                break;

            case 1:
                this.blood1.visible = true;
                this.matBlood1.opacity = 1;
                gsap.to(this.matBlood1, {
                    duration: 1,
                    delay: 3.5, opacity: 0, onComplete: () => {
                        this.blood1.visible = false;
                    }
                });

                break;

            case 2:
                this.blood2.visible = true;
                this.matBlood2.opacity = 1;
                gsap.to(this.matBlood2, {
                    duration: 1,
                    delay: 3.5, opacity: 0, onComplete: () => {
                        this.blood2.visible = false;
                    }
                });

                break;

        }

    }


}