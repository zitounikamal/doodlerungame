import assets from "app/assets";
import SolidObject from "app/modules/common/solidObject";
import config from "config";
import gsap from "gsap/all";
import { BoxBufferGeometry, Mesh, MeshBasicMaterial, Object3D, PlaneBufferGeometry, Vector2, Vector3 } from "three";
import { DEG2RAD } from "three/src/math/MathUtils";
import * as CANNON from 'cannon-es';

export default class Blade2 extends Object3D {

    constructor() {

        super();

        this.update = this.update.bind(this);

        this._type = "chopper2";
        this._dummyRotation = new Object3D();
        this._lame = assets.getAsset("lame2");
        this._axe = assets.getAsset("piege4");

        this.build();

    }


    build() {



        this._lame.visible = true;
        this._lame.position.set(0, .03, -.15);
        this.add(this._lame);

        this._axe.scale.set(1, 1, 1);
        this._axe.position.set(0, 0, 0);
        this._axe.visible = true;
        this.add(this._axe);




        var x = .2;
        var y = .1;
        var z = .2;

        this.mesh = new Mesh(new BoxBufferGeometry(x, y, z));

        this.box = new SolidObject(
            this.mesh,
            new CANNON.Box(new CANNON.Vec3(x / 2, y / 2, z / 2)),
            new Vector3(0, 0, 0)
        );

        this.box.rigideBody = true;
        this.box.isRendred = false;
        this.box.addEventListener("ready", () => {

            this.box.collider.name = this._type;
            this.box.collider.trap = this;
        });

        this.add(this.box);


        this.position.y = .1;


    }



    start = (time, delay) => {

        gsap.to(this._lame.position, {
            duration: time,
            delay: delay,
            z: -1,
            ease: "back.out(2)",
            onComplete: () => {
                gsap.to(this._lame.position, {
                    duration: time * 3,
                    z: -.15,
                    ease: "power4.in"
                });
            }
        });

    }



    update() {

        if (this.parent) {

            let point = new Vector3();
            this._lame.localToWorld(point);

            this._dummyRotation && this._dummyRotation.rotation.set(this.parent.rotation.x, this.parent.rotation.y, this.parent.rotation.z)
            this.box.collider && this.box.collider.position.copy(point);
            this.box.collider && this.box.collider.quaternion.copy(this._dummyRotation.quaternion);
        }

    }







}