import assets from "app/assets";
import SolidObject from "app/modules/common/solidObject";
import { BoxBufferGeometry, BoxGeometry, Color, Mesh, Object3D, Quaternion, Vector3 } from "three";
import tools from "tools";
import * as CANNON from 'cannon-es';
import { DEG2RAD } from "three/src/math/MathUtils";
import Blade from "./blade";
import gsap from "gsap/all";

export default class Chopper extends Object3D {


    constructor(name) {
        super();

        this.build = this.build.bind(this);
        this.startEngine = this.startEngine.bind(this);

        this.name = name;
        this._type = "chopper";
        this._axe = assets.getAsset("piege1");
        this._lame0 = new Blade();
        this._lame1 = new Blade();
        this._lame2 = new Blade();
        this._delay = 1;
        this._state = "close";
        this._callBacks = [];
        this._dummyRotation = new Object3D();


        this.build();

    }



    build() {

        this.scale.set(1.2, 1.2, 1.2);
        this._axe.scale.set(1, 1, 1);
        this._axe.position.set(0, 0, 0);
        this._axe.visible = true;
        this.add(this._axe);

        this._lame0.rotation.y = DEG2RAD * -180;
        this.add(this._lame0);

        this._lame1.rotation.y = DEG2RAD * -180;
        this._lame1.position.y = -.06;
        this.add(this._lame1);

        this._lame2.rotation.y = DEG2RAD * -180;
        this._lame2.position.y = -.12;
        this.add(this._lame2);


        var x = 1;
        var y = .2;
        var z = .15;


        this.mesh = new Mesh(new BoxBufferGeometry(x, y, z));

        this.box = new SolidObject(
            this.mesh,
            new CANNON.Box(new CANNON.Vec3(x / 2, y / 2, z / 2)),
            new Vector3(.7, 0, 0.1)
        );
        this.box.rigideBody = true;
        this.box.isRendred = false;
        this.box.addEventListener("ready", () => {

            this.box.collider.name = this._type;
            this.box.collider.position.x = this.position.x;
            this.box.collider.position.z = this.position.z;
            this.box.collider.position.y = .08;
            this.box.collider.trap = this;
        });

        this.add(this.box);


    }


    startEngine(d) {

        let time = 1.3;
        let delay = d > 0 ? d : this._delay;


        gsap.to(this._lame0.rotation, {
            duration: time,
            delay: .15,
            y: DEG2RAD * -90,
            ease: "elastic.out(0.4, 0.3)",
            onComplete: () => {

                gsap.to(this._lame0.rotation, {
                    duration: time,
                    delay: delay + .15,
                    y: DEG2RAD * -180,
                    ease: "elastic.out(0.4, 0.3)",
                    onComplete: this.startEngine,
                    onStart: () => {
                        //  dispatch("open_" + name);
                    }
                });


                setTimeout(() => {
                    this.dispatch("open_" + this.name);
                    this._state = "open";
                }, time * 900);

                gsap.to(this._lame1.rotation, {
                    duration: time,
                    delay: delay + .1,
                    y: DEG2RAD * -180,
                    ease: "elastic.out(0.4, 0.3)"
                });

                gsap.to(this._lame2.rotation, {
                    duration: time,
                    delay: delay,
                    y: DEG2RAD * -180,
                    ease: "elastic.out(0.4, 0.3)"
                });

                gsap.to(this._axe.rotation, {
                    duration: time,
                    delay: delay,
                    y: DEG2RAD * -180,
                    ease: "elastic.out(0.4, 0.3)"
                });


            },
            onStart: () => {
                this._state = "close";
                this.dispatch("close_" + this.name);
            }
        });

        gsap.to(this._lame1.rotation, {
            duration: time,
            delay: .1,
            y: DEG2RAD * -90,
            ease: "elastic.out(0.4, 0.3)"
        });

        gsap.to(this._lame2.rotation, {
            duration: time,
            delay: 0,
            y: DEG2RAD * -90,
            ease: "elastic.out(0.4, 0.3)"
        });

        gsap.to(this._axe.rotation, {
            duration: time,
            delay: 0,
            y: DEG2RAD * -90,
            ease: "elastic.out(0.4, 0.3)"
        });


    }





    dispatch(event) {

        console.log("debug ", event)

    }



    start(delay) {
        this.startEngine(delay);
    }



    setDelay(val) {
        this._delay = val;
    }



    get state() {
        return this._state;
    }


    showBlood(id) {

        this._lame0.showBlood(id);
        this._lame1.showBlood(id);
        this._lame2.showBlood(id);

    }



    update() {

        this._dummyRotation.rotation.set(this._lame2.rotation.x, this._lame2.rotation.y + this.rotation.y + 180 * DEG2RAD, this._lame0.rotation.z)
        this.box.collider && this.box.collider.quaternion.copy(this._dummyRotation.quaternion);

    }


    dispose() {
      

        tools.clean(this);
    }






}