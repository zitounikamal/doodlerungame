import assets from "app/assets";
import config from "config";
import { EventDispatcher } from "three";

export default class LevelManager extends EventDispatcher {



    constructor() {

        super();

        this._levelID = config.levelID;
        this.next = this.next.bind(this);


    }


    get levelID() {
        return this._levelID;
    }


    next() {

        if (this._levelID < assets.levelsData.length - 1) {

            this._levelID++;

        } else {

            this._levelID = 0;
        }

    }


    update() {



    }



}