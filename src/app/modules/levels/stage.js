import { BoxBufferGeometry, Color, Mesh, Object3D, Vector2, Vector3 } from 'three';

import config from 'config';
import tools from 'tools';
import gsap from 'gsap/all';
import Doodle from '../players/doodle';
import Segment from './blocs/segment';
import Start from './blocs/start';
import assets from 'app/assets';
import { DEG2RAD } from 'three/src/math/MathUtils';
import Right from './blocs/right';
import Left from './blocs/left';
import { degToRad } from 'three/src/math/MathUtils';
import Chopper from './traps/chopper';
import End from './blocs/end';
import Chopper2 from './traps/chopper2';



export default class Stage extends Object3D {

    constructor() {

        super();



        this.update = this.update.bind(this);
        this.initMaterial = this.initMaterial.bind(this);
        this.build = this.build.bind(this);
        this.startScreen = this.startScreen.bind(this);
        this.startGame = this.startGame.bind(this);
        this._mousedown = this._mousedown.bind(this);
        this._mouseup = this._mouseup.bind(this);

        this._directionBloc = new Vector3(0, 0, -1);
        this._decalBloc = new Vector3(0, 0, 0);

        config.segments = this.segments = [];
        config.traps = this.traps = [];

        this.initMaterial();
        !config.isDebug && this.initEvents();
        this.build();

        console.log("* Stage *");


    }


    initEvents() {

        config.app.addEventListener("mousedown", this._mousedown);
        config.app.addEventListener("mouseup", this._mouseup);

    }


    initMaterial() {


    }




    build() {


        // this.text3D = tools.getTextShape(null, assets.font3d, 0xffffff, .2, 1, true);
        // this.add(this.text3D);

        config.ref = this.ref = new Mesh(new BoxBufferGeometry(.01, 1, .01));
        this.ref.material.color = new Color(0xff0000);
        // this.add(this.ref);


        this.level = assets.getAsset("level");
        this.level.rotation.y = 180 * DEG2RAD;
        this.add(this.level);

        let levelID = config.levelManager.levelID;

        let posz = -.25;
        let posx = 0;
        let rot = 0;
        let i;
        let arr = assets.levelsData[levelID].segments;
        let traps = assets.levelsData[levelID].traps;
        let trap;



        //loop segments
        for (i = 0; i < arr.length; i++) {

            switch (arr[i]) {

                case "start":

                    this.segment = new Start("seg" + tools.numberFormat(i, 3), { ...this._directionBloc });
                    this.segment.position.z = posz;
                    this.segment.position.x = posx;

                    posz += this._directionBloc.z * this.segment.size.z;
                    posx += this._directionBloc.x * this.segment.size.x;

                    break;


                case "end":

                    this.segment = new End("seg" + tools.numberFormat(i, 3), { ...this._directionBloc });
                    this.segment.position.z = posz;
                    this.segment.position.x = posx;

                    posz += this._directionBloc.z * this.segment.size.z;
                    posx += this._directionBloc.x * this.segment.size.x;

                    break;


                case "platform":

                    this.segment = new Segment("seg" + tools.numberFormat(i, 3), { ...this._directionBloc });
                    this.segment.position.z = posz;
                    this.segment.position.x = posx;

                    posz += this._directionBloc.z * this.segment.size.z;
                    posx += this._directionBloc.x * this.segment.size.x;

                    break;


                case "right":

                    this.segment = new Right("seg" + tools.numberFormat(i, 3), { ...this._directionBloc }, posx, posz);
                    config.directionBloc = this._directionBloc = this.segment.directionBloc;
                    this._decalBloc = this.segment.decalBloc;

                    posz += this._directionBloc.z * this.segment.size.z + this._decalBloc.z;
                    posx += this._directionBloc.x * this.segment.size.x + this._decalBloc.x;
                    posz -= .1;


                    break;

                case "left":

                    this.segment = new Left("seg" + tools.numberFormat(i, 3), { ...this._directionBloc }, posx, posz);
                    config.directionBloc = this._directionBloc = this.segment.directionBloc;
                    this._decalBloc = this.segment.decalBloc;

                    posz += this._directionBloc.z * this.segment.size.z + this._decalBloc.z;
                    posx += this._directionBloc.x * this.segment.size.x + this._decalBloc.x;
                    posz -= .1;

                    break;

            }

            this.segments.push(this.segment);
            this.add(this.segment);

        }


        //loop traps
        for (i = 0; i < traps.length; i++) {

            switch (traps[i].type) {

                case "trap0":

                    trap = new Chopper(i);
                    trap.position.x = parseFloat(traps[i].x);
                    trap.position.z = parseFloat(traps[i].z);
                    trap.rotation.y = parseFloat(traps[i].rotation) * DEG2RAD;
                    trap.start(traps[i].delay);
                    this.traps.push(trap);
                    this.add(trap);

                    break;


                case "trap1":

                    trap = new Chopper2(i, parseInt(traps[i].nbr));
                    trap.position.x = parseFloat(traps[i].x);
                    trap.position.z = parseFloat(traps[i].z);
                    trap.rotation.y = parseFloat(traps[i].rotation) * DEG2RAD;
                    trap.start(traps[i].delay);
                    this.traps.push(trap);
                    this.add(trap);

                    break;

            }
        }



        config.doodle0 = this.doodle0 = new Doodle(0, "doodle0", config.player0Color, -.25, 1);
        this.add(this.doodle0);

        config.doodle1 = this.doodle1 = new Doodle(1, "doodle1", config.player1Color, 0, config.playerSpeedTime);
        this.add(this.doodle1);

        config.doodle2 = this.doodle2 = new Doodle(2, "doodle2", config.player2Color, .25, 1.02);
        this.add(this.doodle2);


    }



    _mousedown() {

        if (config.gameOver || !config.gameReady) {
            return;
        }

        this._isDown = true;

        if (!config.startGame) {
            config.startGame = true;
            this.startGame();
            config.doodle0.progress = true;
            config.doodle2.progress = true;
        }

        this.doodle1.progress = true;

    }


    _mouseup() {

        if (config.gameOver || !config.gameReady) {
            return;
        }

        this._isDown = false;
        this.doodle1.progress = false;

        // config.doodle0.progress = false;
        // config.doodle2.progress = false;
    }




    startScreen() {

        console.log("startScreen");

        // this.text3D && (this.text3D.rotation.y -= .005);

    }




    startGame() {

        // Launch the game
        console.log("debug startGame");


    }



    dispose() {


        config.app.removeEventListener("mousedown", this._mousedown);
        config.app.removeEventListener("mouseup", this._mouseup);

        this.doodle0 && this.doodle0.dispose();
        this.doodle1 && this.doodle1.dispose();
        this.doodle2 && this.doodle2.dispose();



        for (let i = 0; i < this.segments.length; i++) {
            this.segments[i].dispose();
        }

        for (let i = 0; i < this.traps.length; i++) {
            this.traps[i].dispose();
        }

        tools.clean(this);

    }


    update(delta, viewSize) {

        if (!config.startGame) {
            this.startScreen();
        }

        this.doodle0 && this.doodle0.update(delta);
        this.doodle1 && this.doodle1.update(delta);
        this.doodle2 && this.doodle2.update(delta);

        for (let i = 0; i < this.segments.length; i++) {
            this.segments[i].update(delta);
        }

        for (let i = 0; i < this.traps.length; i++) {
            this.traps[i].update(delta);
        }


    }


}