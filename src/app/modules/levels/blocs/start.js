import assets from "app/assets";
import * as CANNON from 'cannon-es';
import SolidObject from "app/modules/common/solidObject";
import { DoubleSide, MeshStandardMaterial, SpriteMaterial, Vector3 } from "three";
import tools from "tools";
import config from "config";
import { DEG2RAD } from "three/src/math/MathUtils";

const SIZE = new Vector3(1.2, 1, 1);


export default class Start extends SolidObject {

    constructor(name, direction) {

        super(
            assets.getAsset("start"),
            new CANNON.Box(new CANNON.Vec3(SIZE.x / 2, SIZE.y / 2, SIZE.z / 2))
        );

        this.name = name;
        this._type = "start";
        this.direction = direction;
        this.rigideBody = true;
        this.receiveShadow = true;
        this.castShadow = true;
        this._cameraPos = new Vector3(0, 1, 1.6);
        this._cameraFocus = new Vector3(.2 * direction.x, 0, .2 * direction.z);
        this._cameraBehaviorSpeed = 2;

        this.initMaterial();
        this.build();

    }


    initMaterial() {

        this.material = new MeshStandardMaterial({
            metalness: .1,
            roughness: .8,
            map: assets.textures.start,
            envMap: assets.textures.envMap,
            envMapIntensity: .4,
            side: DoubleSide
        });

        this.material.userData.outlineParameters = {
            visible: false
        };

    }



    build() {

        console.log(this.segmentType, ":", this.direction);

        this.position.y = -.5;
        this.rotation.y = DEG2RAD * this.direction.y;

    }

    get segmentType() {
        return this._type;
    }

    get size() {
        return SIZE;
    }


    get destination() {
        return new Vector3(this.position.x + SIZE.x / 2 * this.direction.x, 0, this.position.z + SIZE.z / 2 * this.direction.z);
    }


    get speedTime() {
        return .5;
    }

    get center() {
        return { x: 0, z: 0 };
    }

    get cameraPos() {
        return this._cameraPos;
    }

    get cameraBehaviorSpeed() {
        return this._cameraBehaviorSpeed;
    }

    get cameraFocus() {
        return this._cameraFocus;
    }



    dispose() {

        tools.clean(this);

    }



    update() {


    }


}