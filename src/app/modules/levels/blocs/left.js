import assets from "app/assets";
import * as CANNON from 'cannon-es';
import SolidObject from "app/modules/common/solidObject";
import { DoubleSide, MeshStandardMaterial, Vector3 } from "three";
import tools from "tools";
import { DEG2RAD } from "three/src/math/MathUtils";

const SIZE = new Vector3(1.1, 1, 1.1);


export default class Left extends SolidObject {

    constructor(name, direction, x, z) {

        let mesh = assets.getAsset("left");


        super(
            mesh,
            new CANNON.Box(new CANNON.Vec3(SIZE.x / 2, SIZE.y / 2, SIZE.z / 2))
        );

        this._type = "left";
        this.name = name;
        this.direction = direction;
        this.x = x;
        this.z = z;
        this.rigideBody = true;
        this.receiveShadow = true;
        this.castShadow = true;
        this.centerX = 0;
        this.centerZ = 0;
        this._startAngle = 0;
        this._endAngle = 0;
        this._directionBloc = new Vector3();
        this._decalBloc = new Vector3();
        this._cameraPos = new Vector3(1.5, 1.5, 1.8);
        this._cameraFocus = new Vector3(0, 0, 0);
        this._cameraBehaviorSpeed = 2.5;
        this._speedTime = 1;



        this.initMaterial();
        this.build();

    }


    initMaterial() {

        this.material = new MeshStandardMaterial({
            metalness: .1,
            roughness: .8,
            map: assets.textures.motif,
            envMap: assets.textures.envMap,
            envMapIntensity: .4,
            side: DoubleSide
        });

        this.material.userData.outlineParameters = {
            visible: false
        };

    }



    build() {

        this.rotation.y = this.direction.y * DEG2RAD;

        console.log(this.segmentType, ":", this.direction);

        switch (this.direction.y) {

            case 0:
                this.position.z = this.z - .05;
                this.position.x = this.x - .05;
                this.centerX = this.position.x - SIZE.x / 2;
                this.centerZ = this.position.z + SIZE.z / 2;
                this._startAngle = 0;
                this._endAngle = -90;
                this._directionBloc.set(-1, 90, 0);
                this._decalBloc.set(0, 0, 0);
                break;

            case 90:
                this.position.z = this.z + .05;
                this.position.x = this.x - .05;
                this.centerX = this.position.x + SIZE.x / 2;
                this.centerZ = this.position.z + SIZE.z / 2;
                this._startAngle = -90;
                this._endAngle = -180;
                this._directionBloc.set(0, 180, 1);
                this._decalBloc.set(-.1, 0, .1);
                break;

            case -90:
                this.position.z = this.z - .05;
                this.position.x = this.x + .05;
                this.centerX = this.position.x - SIZE.x / 2;
                this.centerZ = this.position.z - SIZE.z / 2;
                this._startAngle = 90;
                this._endAngle = 0;
                this._directionBloc.set(0, 0, -1);
                this._decalBloc.set(.1, 0, .1);
                break;


            case 180:
                this.position.z = this.z + .05;
                this.position.x = this.x + .05;
                this.centerX = this.position.x + SIZE.x / 2;
                this.centerZ = this.position.z - SIZE.z / 2;
                this._startAngle = 180;
                this._endAngle = 90;
                this._directionBloc.set(1, -90, 0);
                this._decalBloc.set(0, 0, .2);
                break;

        }


        this.position.y = -.5;

    }


    get segmentType() {
        return this._type;
    }


    get center() {
        return { x: this.centerX, z: this.centerZ };
    }

    get radius() {
        return SIZE.z / 2 + .05;
    };

    get startAngle() {
        return this._startAngle;
    };

    get endAngle() {
        return this._endAngle;
    };


    get size() {
        return SIZE;
    }


    get destination() {
        return new Vector3(this.position.x, 0, this.position.z);
    }

    get speedTime() {
        return this._speedTime;
    }

    get directionBloc() {
        return this._directionBloc;
    }

    get decalBloc() {
        return this._decalBloc;
    }

    get cameraPos() {
        return this._cameraPos;
    }

    get cameraFocus() {
        return this._cameraFocus;
    }

    get cameraBehaviorSpeed() {
        return this._cameraBehaviorSpeed;
    }



    dispose() {

        tools.clean(this);

    }



    update() {


    }


}