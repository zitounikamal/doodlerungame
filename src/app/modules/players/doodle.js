import { AnimationMixer, BoxBufferGeometry, Clock, Color, DoubleSide, LoopOnce, Mesh, MeshBasicMaterial, MeshPhongMaterial, MeshStandardMaterial, MeshToonMaterial, Object3D, PlaneBufferGeometry, Sprite, SpriteMaterial, Vector2, Vector3 } from "three";
import gsap from "gsap";
import assets from "app/assets";
import { clone as CloneGltfScene } from "lib/SkeletonUtils";
import tools from "tools";
import config from "config";
import { DEG2RAD } from "three/src/math/MathUtils";
import { RAD2DEG } from "three/src/math/MathUtils";
import SolidObject from "../common/solidObject";
import * as CANNON from 'cannon-es';

const SIZE = new Vector3(.05, .2, .05);


export default class Doodle extends Object3D {

    constructor(id, name, color, decal, speedCoef) {

        super();

        this._id = id;
        this.name = name;
        this.color = color;
        this.decal = decal;
        this.speedCoef = speedCoef;

        this._build = this._build.bind(this);
        this.stopAction = this.stopAction.bind(this);
        this.runAction = this.runAction.bind(this);
        this.victoryAction = this.victoryAction.bind(this);
        this.crawleAction = this.crawleAction.bind(this);
        this.crawling = this.crawling.bind(this);
        this.onMixerFinsh = this.onMixerFinsh.bind(this);
        this.update = this.update.bind(this);
        this.move = this.move.bind(this);
        this.rotate = this.rotate.bind(this);
        this.onCollide = this.onCollide.bind(this);
        this.getPartBody = this.getPartBody.bind(this);
        this.reborn = this.reborn.bind(this);
        this._initMaterials = this._initMaterials.bind(this);
        this._ready = this._ready.bind(this);

        this._endTime = 3;
        this._mainScale = 1.2;
        this._crawlingSpeed = .35;
        this._runSpeed = 1;
        this._isCrawling = false;
        this._timeScale = 1;
        this._progress = false;
        this._oldSegments = "";
        this._focus = new Vector3();
        this.decalFocusX = 0;
        this.decalFocusZ = 0;
        this.rotationProp = 0;
        this.datas = { cameraFocus: { x: 0, y: 0, z: 0 } };
        this._cutAccess = true;
        this._go = false;
        this.updateAccess = true;


        this._build();


    }



    _build() {

        console.log("Doodle");

        this.clock = new Clock();

        this._initMaterials();

        this._parseGltfData(assets.gltf.doodle);

        this._initPhysics();

        if (this.name == "doodle1") {
            config.camera.position.set(0, 1, 1);
            this.add(config.cameraContainer);
        }


        this.interval = setInterval(() => {
            if (config.stage) {
                clearInterval(this.interval);
                this._ready();
            }
        }, 60);


    }




    _ready() {
        console.log("doodle", this._id, "ready")
        config.stage.add(this.bloodSprite0);
        config.stage.add(this.bloodSprite1);
        config.stage.add(this.bloodSprite2);
        config.stage.add(this.trainee);

    }



    _initMaterials() {

        this.faceMat = new MeshBasicMaterial({
            map: assets.textures.faceMap,
            transparent: true
        });


        this.whiteMat = new MeshStandardMaterial({
            metalness: 0.2,
            roughness: 0.45,
            envMap: assets.textures.envMap,
            envMapIntensity: 1,
            side: DoubleSide
        });


        this.doodlePartMat = new MeshStandardMaterial({
            color: this.color,
            metalness: 0.2,
            roughness: 0.45,
            envMap: assets.textures.envMap,
            envMapIntensity: 1,
            side: DoubleSide
        });


        this.doodleMat = new MeshStandardMaterial({
            color: this.color,
            metalness: 0.2,
            roughness: 0.45,
            envMap: assets.textures.envMap,
            envMapIntensity: 1,
            side: DoubleSide
        });


        let material = new SpriteMaterial({
            map: assets.textures.bloodImpact0,
            color: this.color,
            transparent: true,
            opacity: 0,
            depthTest: false
        });

        this.bloodSprite0 = new Sprite(material);
        this.bloodSprite0.position.y = .1;
        this.bloodSprite0.visible = false;


        material = new SpriteMaterial({
            map: assets.textures.bloodImpact1,
            color: this.color,
            transparent: true,
            opacity: 0,
            depthTest: false
        });

        this.bloodSprite1 = new Sprite(material);
        this.bloodSprite1.position.y = .1;
        this.bloodSprite1.visible = false;




        this.matBloodImpact = new MeshBasicMaterial({
            color: this.color,
            depthWrite: false
        });
        this.matBloodImpact.transparent = true;
        this.matBloodImpact.map = assets.textures.bloodImpact2;

        this.bloodSprite2 = new Mesh(new PlaneBufferGeometry(.5, .5), this.matBloodImpact);
        this.bloodSprite2.rotation.x = DEG2RAD * 90;
        this.bloodSprite2.rotation.y = DEG2RAD * 180;
        this.bloodSprite2.visible = false;
        this.bloodSprite2.position.y = .01 + .02 * Math.random();





        this.matBloodImpact3 = new MeshBasicMaterial({
            color: this.color,
            depthWrite: false,
            opacity: .5,
            transparent: true,
            map: assets.textures.bloodImpact3
        });

        this.bloodSprite3 = new Mesh(new PlaneBufferGeometry(.25, .25), this.matBloodImpact3);

        this.traineeArr = [];
        this.trainee = tools.getInstances(this.bloodSprite3, this.traineeArr, 100);
        this.trainee.visible = true;





    }





    _parseGltfData(gltf) {


        let _scene = CloneGltfScene(gltf.scene);
        let _object;

        this.wire_r = _scene.getObjectByName("wire_r");
        this.wire_r.material = this.whiteMat;
        this.wire_r.receiveShadow = true;
        this.wire_r.castShadow = true;

        this.snicker_r1 = _scene.getObjectByName("snicker_r_1");
        this.snicker_r1.material = this.doodleMat;
        this.snicker_r1.receiveShadow = true;
        this.snicker_r1.castShadow = true;

        this.snicker_r2 = _scene.getObjectByName("snicker_r_2");
        this.snicker_r2.material = this.doodleMat;
        this.snicker_r2.receiveShadow = true;
        this.snicker_r2.castShadow = true;

        this.wire_l = _scene.getObjectByName("wire_l");
        this.wire_l.material = this.whiteMat;
        this.wire_l.receiveShadow = true;
        this.wire_l.castShadow = true;

        this.snicker_l1 = _scene.getObjectByName("snicker_l_1");
        this.snicker_l1.material = this.doodleMat;
        this.snicker_l1.receiveShadow = true;
        this.snicker_l1.castShadow = true;

        this.snicker_l2 = _scene.getObjectByName("snicker_l_2");
        this.snicker_l2.material = this.doodleMat;
        this.snicker_l2.receiveShadow = true;
        this.snicker_l2.castShadow = true;

        this.face2 = _scene.getObjectByName("face2");
        this.face2.material = this.faceMat;
        this.face2.visible = false;



        _scene.children.map((object) => {

            _object = tools.parseObject(object);



            if (_object) {

                switch (_object.name) {


                    case "doodle":
                        _object.material = this.doodleMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.doodle = _object;

                        break;


                    case "face":
                        _object.material = this.faceMat;
                        this.face = _object;
                        break;



                    case "crawle":
                        _object.material = this.doodleMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        this.crawleObj = _object;
                        this.crawleObj.visible = false;
                        break;



                    case "part0":
                        _object.material = this.doodlePartMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        _object.position.set(0, 0, 0);

                        this.part0 = _object.clone();
                        this.part0.scale.set(this._mainScale, this._mainScale, this._mainScale);
                        this.part0.visible = true;
                        // config.scene.add(this.part0);

                        _object.visible = false;

                        break;


                    case "part1":
                        _object.material = this.doodlePartMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        _object.position.set(0, 0, 0);

                        this.part1 = _object.clone();
                        this.part1.scale.set(this._mainScale, this._mainScale, this._mainScale);
                        this.part1.visible = true;
                        // config.scene.add(this.part1);

                        _object.visible = false;

                        break;

                    case "part2":
                        _object.material = this.doodlePartMat;
                        _object.receiveShadow = true;
                        _object.castShadow = true;
                        _object.position.set(0, 0, 0);

                        this.part2 = _object.clone();
                        this.part2.scale.set(this._mainScale, this._mainScale, this._mainScale);
                        this.part2.visible = true;
                        // config.scene.add(this.part2);

                        this.part3 = _object.clone();
                        this.part3.scale.set(1, 1, 1);
                        this.part3.visible = true;
                        // config.scene.add(this.part3);

                        _object.visible = false;

                        break;



                }
            }


        });



        let animations = gltf.animations;


        if (animations.length > 0) {

            this.mixer = new AnimationMixer(_scene);
            this.mixer.addEventListener('finished', this.onMixerFinsh);

            this.run = this.mixer.clipAction(animations[0]);
            this.run.timeScale = this._timeScale;


            this.stop = this.mixer.clipAction(animations[1]);
            this.stop.loop = LoopOnce;
            this.stop.clampWhenFinished = true;
            this.stop.timeScale = this._timeScale * 0.5;


            this.victory = this.mixer.clipAction(animations[2]);
            this.victory.timeScale = this._timeScale * 1.5;


            this.crawle = this.mixer.clipAction(animations[3]);
            this.crawle.loop = LoopOnce;
            this.crawle.clampWhenFinished = true;
            this.crawle.timeScale = this._timeScale * 1.5;


        }

        _scene.position.y = 0.005;
        _scene.position.x = this.decal;
        _scene.rotation.y = DEG2RAD * 180;
        _scene.scale.set(this._mainScale, this._mainScale, this._mainScale);

        this._scene = _scene;

        this.add(_scene);



    }









    _initPhysics() {


        var x = .1;
        var y = .12;
        var z = .1;


        this.hitBox = new SolidObject(
            null,
            new CANNON.Sphere(y),
            new Vector3(this.decal, .2, 0)
        );
        this.hitBox.rigideBody = true;
        this.hitBox.isRendred = false;
        this.hitBox.addEventListener("ready", () => {

            this.hitBox.collider.position.x = this.position.x;
            this.hitBox.collider.position.z = this.position.z;
            this.hitBox.collider.position.y = y / 2;

            this.hitBox.collider.type = CANNON.Body.DYNAMIC;
            this.hitBox.collider.addEventListener("collide", this.onCollide);
        });

        this.add(this.hitBox);




    }









    onCollide(e) {

        // console.log(e, this._cutAccess);

        if (this._cutAccess) {

            this._cutAccess = false;

            e.body.trap && e.body.trap.showBlood && e.body.trap.showBlood(this._id);


            switch (e.body.name) {

                case "chopper":

                    this.crawleAction(0);

                    break;


                case "chopper2":

                    this.crawleAction(1);

                    break;

            }






            // Recovry
            this.recovryCutAccessTimer && this.recovryCutAccessTimer.kill();
            this.recovryCutAccessTimer = gsap.delayedCall(5, () => {
                this._cutAccess = true;
                console.log("_cutAccess true");
            });



        }



    }










    stopAction() {

        this.run && this.run.stop();
        this.victory && this.victory.stop();
        this.crawle && this.crawle.stop();
        this.stop && this.stop.play();

        this.crawleObj.visible = false;
        this.face2.visible = false;
        this.doodle.visible = true;
        this.snicker_l1.visible = true;
        this.snicker_l2.visible = true;
        this.snicker_r1.visible = true;
        this.snicker_r2.visible = true;
        this.wire_r.visible = true;
        this.wire_l.visible = true;
        this.face.visible = true;


    }








    runAction() {

        this.run && this.run.play();
        this.victory && this.victory.stop();
        this.crawle && this.crawle.stop();
        this.stop && this.stop.stop();

        this.crawleObj.visible = false;
        this.face2.visible = false;
        this.doodle.visible = true;
        this.snicker_l1.visible = true;
        this.snicker_l2.visible = true;
        this.snicker_r1.visible = true;
        this.snicker_r2.visible = true;
        this.wire_r.visible = true;
        this.wire_l.visible = true;
        this.face.visible = true;



    }






    victoryAction() {

        this.run && this.run.stop();
        this.victory && this.victory.play();
        this.crawle && this.crawle.stop();
        this.stop && this.stop.stop();

        this.crawleObj.visible = false;
        this.face2.visible = false;
        this.doodle.visible = true;
        this.snicker_l1.visible = true;
        this.snicker_l2.visible = true;
        this.snicker_r1.visible = true;
        this.snicker_r2.visible = true;
        this.wire_r.visible = true;
        this.wire_l.visible = true;
        this.face.visible = true;

    }



    crawleAction(type) {

        this._isCrawling = true;

        this.name != "doodle1" && (this.progress = true);

        this.run && this.run.stop();
        this.victory && this.victory.stop();
        this.stop && this.stop.stop();

        this.crawleObj.visible = true;
        this.face2.visible = true;
        this.doodle.visible = false;
        this.snicker_l1.visible = false;
        this.snicker_l2.visible = false;
        this.snicker_r1.visible = false;
        this.snicker_r2.visible = false;
        this.wire_r.visible = false;
        this.wire_l.visible = false;
        this.face.visible = false;


        this.crawling(type);


    }










    crawling(type) {


        this.moveTween && this.moveTween.timeScale(this._isCrawling ? this._crawlingSpeed : this._runSpeed);

        this._scene.position.y = .1;

        gsap.to(this._scene.position, { duration: .1, y: .2 });
        gsap.to(this._scene.position, {
            duration: .25,
            delay: .2,
            y: 0,
            onComplete: () => {
                this.crawle.play();
            }
        });

        const coef = .6;

        let point = new Vector3();
        this._scene.localToWorld(point);

        this.bloodSprite0.position.x = point.x;
        this.bloodSprite0.position.z = point.z;
        this.bloodSprite0.visible = true;
        this.bloodSprite0.material.opacity = 1;
        this.bloodSprite0.material.rotation = DEG2RAD * 360 * Math.random();


        this.bloodSprite0.scale.set(.7, .7);
        gsap.to(this.bloodSprite0.scale, { duration: 1 * coef, x: 1.5, y: 1.5 });

        gsap.to(this.bloodSprite0.material, {
            delay: .1, duration: .3 * coef, opacity: 0
        });




        this.bloodSprite1.position.x = point.x;
        this.bloodSprite1.position.z = point.z;
        this.bloodSprite1.visible = true;
        this.bloodSprite1.material.opacity = 1;
        this.bloodSprite1.material.rotation = DEG2RAD * 360 * Math.random();

        this.bloodSprite1.scale.set(.6, .6);
        gsap.to(this.bloodSprite1.scale, { delay: .1, duration: 2 * coef, x: 1.5, y: 1.5 });

        gsap.to(this.bloodSprite1.material, {
            delay: .2, duration: .6 * coef, opacity: 0, onComplete: () => {
                this.bloodSprite0.visible = false;
                this.bloodSprite1.visible = false;
            }
        });


        this.bloodSprite2.position.x = point.x;
        this.bloodSprite2.position.z = point.z;
        this.bloodSprite2.visible = true;






        switch (type) {

            case 0:

                this.part0Collider && (this.part0Collider.visible = false);
                this.part1Collider && (this.part1Collider.visible = false);
                this.part2Collider && (this.part2Collider.visible = false);

                this.part2Collider = this.getPartBody(this.part2, .06, .06, .06, 0, .4);
                this.part1Collider = this.getPartBody(this.part1, .06, .06, .06, 0, .3);
                this.part0Collider = this.getPartBody(this.part0, .06, .06, .06, 0, .2);

                break;


            case 1:

                this.part0Collider && (this.part0Collider.visible = false);

                this.part0Collider = this.getPartBody(this.part2, .06, .1, .06, 1, .2);

                break;








        }












        this.traineeInterval && clearInterval(this.traineeInterval);
        this.traineeInterval = setInterval(() => {

            let point = new Vector3();
            this._scene.localToWorld(point);

            if (this._isArrowTrap) {

                var scale = Math.random() * 0.15;
                this.traineeArr.push({
                    position: { x: point.x + tools.range(-.12, .12), y: .1, z: point.z + .05 + tools.range(-.12, .12) },
                    rotation: { x: DEG2RAD * 90, y: DEG2RAD * 180, z: this.rotationY * Math.random() },
                    scale: { x: scale, y: scale, z: 1 }
                })

            } else {

                this.traineeArr.push({
                    position: { x: point.x, y: .01, z: point.z },
                    rotation: { x: DEG2RAD * 90, y: DEG2RAD * 180, z: this.rotationY + tools.range(-10, 10) * DEG2RAD },
                    scale: { x: .4, y: .8, z: 1 }
                })

            }

            this.trainee.update(this.traineeArr);


        }, 60);








    }











    reborn(onReset, forced) {


        gsap.to(this.doodlePartMat, {
            duration: .3,
            opacity: 0, onComplete: () => {

                this.part0Collider && (this.part0Collider.visible = false);
                this.part1Collider && (this.part1Collider.visible = false);
                this.part2Collider && (this.part2Collider.visible = false);

                this.part0Collider && (this.part0Collider.rigideBody = false);
                this.part1Collider && (this.part1Collider.rigideBody = false);
                this.part2Collider && (this.part2Collider.rigideBody = false);

                if (this._progress) {
                    this.runAction();
                } else {
                    this.stopAction();
                }

                this._isCrawling = false;
                this.moveTween && this.moveTween.timeScale(this._isCrawling ? this._crawlingSpeed : this._runSpeed);

                this.traineeInterval && clearInterval(this.traineeInterval);
                this.clearTraineeTimer && clearTimeout(this.clearTraineeTimer);

                // this.clearTraineeTimer = setTimeout(() => {
                //     this.traineeInterval && clearInterval(this.traineeInterval);
                //     this.traineeArr = [];
                //     this.trainee.update(this.traineeArr);
                // }, 3000);

            }
        });


        gsap.to(this._scene.scale, { duration: .1, y: 1.4, x: 1.4, z: 1.4 });
        this.scaleDoodleTween && this.scaleDoodleTween.kill();
        this.scaleDoodleTween = gsap.to(this._scene.scale, {
            duration: 1.6,
            delay: .2,
            y: this._mainScale,
            x: this._mainScale,
            z: this._mainScale,
            ease: "elastic.out(2.5, 0.23)",
            onComplete: () => {
                this._cutAccess = true;
            }
        });




    }











    onMixerFinsh(e) {

        if (e.action._clip.name == "crawle") {

            this.reborn();

        }


    }





    move(x, z, time, datas) {

        this.moveTween && this.moveTween.kill();
        this.moveTween = gsap.to(this.position, {
            duration: time * this.speedCoef, x: x, z: z, ease: "none", onComplete: () => {
                if (this.win) {
                    this.progress = false;
                    this.victoryAction();

                    if (this.name == "doodle1") {
                        console.log("WIN");
                        gsap.to(this, { duration: 2, rotationY: this.rotationY + 180 * DEG2RAD });
                        gsap.to(this._scene.rotation, { duration: 2, y: this._scene.rotation.y - 180 * DEG2RAD });

                        gsap.delayedCall(this._endTime, () => {
                            config.app.reset();
                        });

                    }


                }
            }
        });
        !this.progress && this.moveTween.pause();
        this.moveTween && this.moveTween.timeScale(this._isCrawling ? this._crawlingSpeed : this._runSpeed);


    }


    rotate(center, radius, startAngle, endAngle, time, datas) {

        this.rotationProp = startAngle;
        this.rotationY = (datas.rotationVal - this.rotationProp) * DEG2RAD;
        this.moveTween && this.moveTween.kill();
        let pos;
        this.moveTween = gsap.to(this, {
            duration: time * this.speedCoef,
            rotationProp: endAngle,
            ease: "none", onUpdate: () => {
                pos = tools.angle2pos({ x: center.x, y: center.z }, radius, this.rotationProp);
                this.rotationY = (datas.rotationVal - this.rotationProp) * DEG2RAD;
                this.position.x = pos.x;
                this.position.z = pos.y;
            }
        });
        !this.progress && this.moveTween.pause();
        this.moveTween && this.moveTween.timeScale(this._isCrawling ? this._crawlingSpeed : this._runSpeed);



    }









    getPartBody(mesh, x, y, z, type, posy) {


        let box = new SolidObject(
            mesh,
            new CANNON.Box(new CANNON.Vec3(x / 2, y / 2, z / 2)),
        );
        box.visible = false;
        box.rigideBody = true;
        box.isKinematic = false;
        box.castShadow = true;

        box.addEventListener("ready", () => {


            console.log("ready ", this._id)

            let point = new Vector3();
            this._scene.localToWorld(point);

            box.collider.position.set(point.x + Math.random() * .1, posy, point.z + Math.random() * .1);

            gsap.delayedCall(.1, () => {
                box.visible = true;
            });
        });


        config.parts.push(box);
        config.stage.add(box);

        if (type == 1) {
            mesh.rotation.set(0, 0, 0);
        }

        gsap.to(mesh.scale, {
            duration: 0,
            x: 3 * Math.random(),
            y: 3 * Math.random() * (type == 1 ? 3 : 1),
            z: 3 * Math.random()
        })

        gsap.to(mesh.scale, {
            delay: .1,
            duration: 2,
            x: this._mainScale,
            y: this._mainScale * (type == 1 ? 3 : 1),
            z: this._mainScale,
            ease: "elastic.out(2, .4)"
        })

        return box;

    }
















    set rotationY(val) {
        this.rotation.y = val;
    }

    get rotationY() {
        return this.rotation.y;
    }


    set progress(val) {

        this._progress = val;

        if (this._progress) {
            if (!this._go && this.name != "doodle1") {
                gsap.delayedCall(Math.random() * .2, () => {
                    this.moveTween && this.moveTween.resume();
                    !this._isCrawling && this.runAction();
                });
            } else {
                this.moveTween && this.moveTween.resume();
                !this._isCrawling && this.runAction();
            }
            this._go = true;
        } else {
            this.moveTween && this.moveTween.pause();
            !this._isCrawling && this.stopAction();
        }

        this.moveTween && this.moveTween.timeScale(this._isCrawling ? this._crawlingSpeed : this._runSpeed);

    }


    get progress() {

        return this._progress;

    }


    get size() {
        return SIZE;
    }




    dispose() {


        console.log("Doodle " + this.id + " disposed");

        if (this.name == "doodle1") {
            config.scene.add(config.cameraContainer);
            config.camera.position.set(0, 1, 1);
        }

        tools.clean(this.bloodSprite0);
        tools.clean(this.bloodSprite1);
        tools.clean(this.bloodSprite2);
        tools.clean(this.trainee);

        if (this.part0Collider) {
            this.part0Collider.visible = false;
        }

        if (this.part1Collider) {
            this.part1Collider.visible = false;
        }

        if (this.part2Collider) {
            this.part2Collider.visible = false;
        }


        this.traineeArr = [];
        this.trainee.update(this.traineeArr);
        this.trainee.clear();
        this.traineeInterval && clearInterval(this.traineeInterval);

        this._oldSegments = "";

        this.run.stop();
        this.mixer.uncacheClip(this.run);
        this.mixer.uncacheAction(this.run);

        this.stop.stop();
        this.mixer.uncacheClip(this.stop);
        this.mixer.uncacheAction(this.stop);

        this.crawle.stop();
        this.mixer.uncacheClip(this.crawle);
        this.mixer.uncacheAction(this.crawle);

        this.victory.stop();
        this.mixer.uncacheClip(this.victory);
        this.mixer.uncacheAction(this.victory);

        tools.clean(this);


    }




    update(delta) {


        this.mixer.update(this.clock.getDelta());


        if (this.name == "doodle1") {
            this._focus.set(this.position.x + this.datas.cameraFocus.x, this.position.y + this.datas.cameraFocus.y, this.position.z + this.datas.cameraFocus.z);
            config.camera.lookAt(this._focus);
        }

        if (!this.updateAccess || !config.gameReady) {
            return;
        }

        if (config.gameOver && !this.win) {
            this.progress = false;
            this.stopAction();
            this.updateAccess = false;

            if (this.name == "doodle1") {
                console.log("LOSE");

                gsap.delayedCall(this._endTime, () => {
                    config.app.reset();
                });

            }

            return;
        }


        //MOVE & ROTATION

        for (let i = 0; i < config.segments.length; i++) {

            if (tools.hitTest(this, config.segments[i]) && this._oldSegments.indexOf(config.segments[i].name) == -1) {

                this._oldSegments += config.segments[i].name;

                config.ref.position.x = config.segments[i].center.x;
                config.ref.position.z = config.segments[i].center.z;
                this.datas.cameraPos = config.segments[i].cameraPos;
                this.datas.cameraBehaviorSpeed = config.segments[i].cameraBehaviorSpeed;

                this.cameraFocusSpeedTween && this.cameraFocusSpeedTween.kill();
                this.cameraFocusSpeedTween = gsap.to(this.datas.cameraFocus, {
                    duration: 2,
                    x: config.segments[i].cameraFocus.x,
                    y: config.segments[i].cameraFocus.y,
                    z: config.segments[i].cameraFocus.z
                })

                if (config.segments[i].segmentType == "right") {

                    this.datas.rotationVal = 180;

                    this.rotate(config.segments[i].center, config.segments[i].radius, config.segments[i].startAngle, config.segments[i].endAngle, config.segments[i].speedTime, this.datas);

                } else if (config.segments[i].segmentType == "left") {

                    this.datas.rotationVal = 0;

                    this.rotate(config.segments[i].center, config.segments[i].radius, config.segments[i].startAngle, config.segments[i].endAngle, config.segments[i].speedTime, this.datas);

                } else {

                    this.move(config.segments[i].destination.x, config.segments[i].destination.z, config.segments[i].speedTime, this.datas);
                }



                if (this.name == "doodle1") {
                    this.cameraBehaviorTween && this.cameraBehaviorTween.kill();
                    this.cameraBehaviorTween = gsap.to(config.camera.position, {
                        duration: this.datas.cameraBehaviorSpeed,
                        x: this.datas.cameraPos.x,
                        y: this.datas.cameraPos.y,
                        z: this.datas.cameraPos.z
                    });
                }


                if (config.segments[i].segmentType == "end" && !config.gameOver) {
                    config.gameOver = true;
                    this.updateAccess = false;
                    // this.victoryAction();
                    this.win = true;
                }

            }

        }





        let tmp = new Vector3(this.position.x, this.position.y, this.position.z);
        this.hitBox.collider && this.hitBox.collider.position.copy(tmp);
        this.hitBox.collider && this.hitBox.collider.quaternion.copy(this.quaternion);







        //IA

        if (this.name != "doodle1" && this._go) {

            for (let i = 0; i < config.traps.length; i++) {

                if (!this._isCrawling) {
                    if (tools.distance3d(config.traps[i].position, this.position) <= 1) {

                        if (config.traps[i].state == "open") {

                            this.progress = false;
                        } else {
                            this.progress = true;
                        }
                    }

                }

            }




        }


    }



}