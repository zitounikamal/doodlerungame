import { Object3D } from "three";
import tools from "tools";


export default class GameObject extends Object3D {

    constructor() {

        super();

        this._collider = null;

    }


    get collider() {

        return this._collider;

    }

    set collider(data) {

        let interval = setInterval(() => {
            if (this.parent) {
                clearInterval(interval);
                this._collider = tools.addPhysics(this, data.mass, data.shape, data.rotation, data.material, data.offset);
                if (this._collider) {
                    this._collider.allowSleep = true;
                    this._collider.sleepSpeedLimit = 0.5;
                    this._collider.sleepTimeLimit = 2;
                    this.dispatchEvent({ type: "ready" });
                } else {
                    console.warn("CannonJS not initialized");
                }
            }
        }, 60);

    }






}