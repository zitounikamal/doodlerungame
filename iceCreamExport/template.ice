<template>

  <div id="loading">
   <h1 style="margin-bottom: 30px;">{{general.name}}</h1>
   <div class="loader"></div>
  </div>

  {{module.header.html}}
  {{module.footer.html}}
  {{module.startScreen.html}}
  {{module.endScreen.html}}
  {{module.retryScreen.html}}

</template>

<script>

 window._gameplay = {
    creative:{        

        maxScore: '{{creative.maxScore}}',
        background: '{{creative.background}}',
        sound: '{{creative.sound}}'

        
    }
  }

  

  window._gameplayEndLevel = JSON.parse(JSON.stringify(window._gameplay.creative));



  window._gameplayEndLevel.maxScore = '{{endLevel.maxScore}}';

  window._gameplayEndLevel.background = '{{endLevel.background}}';

  window._gameplayEndLevel.sound = '{{endLevel.sound}}';


  window._gameplay.timerEndScreen = {{creative.timerEndScreen}};
  window._gameplay.nbRetry = {{creative.nbRetry}};
  window._gameplay.isEnd = false;
  window._gameplay.timerLock = false;
  window._gameplay.isInit = false;
  window._gameplay.paused = false;
  window._gameplay.nbClick = 0;
  window._gameplay.started = false;
  window._gameplay.redirectionAfterClick = {{creative.redirectionAfterClick}};
  window._gameplay.restartAfterStore = {{endLevel.restartAfterStore}};
	window._gameplay.autoStart = {{advanced.autoStart}};
  window._gameplay.autoStartDelay = {{advanced.autoStartDelay}};

  window._gameplayEvents = {};

//// UPDATE
  window._gameplayEvents.checkAutoStart = function(){
		if(window._gameplay.autoStart){
			setTimeout(function(){
				if(!window._gameplay.started){
					if (typeof window._gameplayEvents.startGame === 'function'){
						window._gameplay.started = true;
						window._gameplayEvents.startGame();
					}
				}
			},window._gameplay.autoStartDelay*1000);
		}
	}
////

  window._gameplayEvents.startTimer = function(event){
    if(window._gameplay.timerLock) return;
    window._gameplay.timerLock = true;
    document.getElementById("start-screen").style.display = "none";
    if(!window._gameplay.started){
			window._gameplay.started = true;
			window._gameplayEvents.startGame();
		}
    setTimeout(function() {
       if(document.getElementById('retry-screen').style.display =="none" || document.getElementById('retry-screen').style.display=="")
          window._gameplayEvents.endGame("timer");
     }, window._gameplay.timerEndScreen*1000);
  };

  window._gameplayEvents.endGame = function(end){
    if(window._gameplay.isEnd) return;
    console.log("End Game : "+ end);
    window._gameplay.isEnd = true;
    if(window._voodooProvider == 'vungle')
      window.parent.postMessage('complete','*');
    window.showEndCard();
    if({{endLevel.available}})
    	window._gameplayEvents.endLevel();
    setTimeout(function(){
      document.getElementById('end-screen').style.display = "flex";
      document.getElementById('end-screen').classList.remove('hide');
      if (typeof window.showEndScreen === 'function') {
        window.showEndScreen();
      }
    },300);
  };

  window._gameplayEvents.endLevel = function(){
		if(window._gameplay.lockEndLevel) return;
		window._gameplay.lockEndLevel = true;
    window._gameplay.isEndLevel = true;
    setTimeout(function(){
      window._gameplay.creative = window._gameplayEndLevel;
      window._gameplayEvents.startGame();
      window._gameplay.paused = {{endLevel.paused}};
			window._gameplay.lockEndLevel = false;
    },{{endLevel.delay}}*1000)
  };

  window._gameplayEvents.endLevelAfterStore = function(type){
		if(window._gameplay.lockEndLevel) return;
		window._gameplay.lockEndLevel = true;
    window._gameplay.isEndLevel = true;
    if(!window._gameplay.restartAfterStore) return;
    document.getElementById('end-screen').style.display = "none";
    var delay = type == "editor"? 500 : 1250;
    setTimeout(function(){
      window._gameplay.creative = window._gameplayEndLevel;
      window._gameplayEvents.startGame();
      window._gameplay.paused = {{endLevel.paused}};
			window._gameplay.lockEndLevel = false;
    },delay)
  };

  window._gameplayEvents.lose = function () {
    if(window._gameplay.isEnd)return;
    if(window._gameplay.nbRetry>0){
      if (typeof window.showRetryScreen === 'function') {
        window.showRetryScreen();
      }
    }else{
      if(window._gameplay.restartAfterStore && window._gameplay.isEndLevel)
				window._gameplayEvents.endLevelAfterStore();
			else
      	window._gameplayEvents.endGame("lose");
    }
  }

  window._gameplayEvents.retry = function () {
    document.getElementById('retry-screen').style.display ="none";
    window._gameplay.nbRetry--;
    window._gameplayEvents.startGame();
  }


  window._gameplayEvents.exit = function(){
      window._gameplay.bannerClick = true;
      window._voodooExit();
      setTimeout(function() {
        window._gameplay.bannerClick = false;
      }, 500);
  }

  window._voodooIOS = "{{general.ios}}";
  window._voodooANDROID = "{{general.android}}";

  var lockClick = false;
	var storeRedirection = function(){
		if(lockClick)return;
		lockClick = true;
    window._gameplay.nbClick ++;
    if(window._gameplay.nbClick >= window._gameplay.redirectionAfterClick && window._gameplay.redirectionAfterClick!=0){
      window._voodooExit("click");
    }
		setTimeout(function(){
			lockClick = false;
		},250)
  }

  document.addEventListener('touchend',storeRedirection);
  document.addEventListener('touchcancel',storeRedirection);
  document.addEventListener('pointerup',storeRedirection);

  {{module.header.js}}
  {{module.footer.js}}
  {{module.startScreen.js}}
  {{module.endScreen.js}}
  {{module.retryScreen.js}}

</script>

<style>
  @font-face {
    font-family: custom;
    src: url("{{advanced.font}}");
  }
  html,
  body {
      width: 100%;
      height: 100%;
      max-height: 100%;
      overflow: hidden;
      margin: 0;
      padding: 0;
      border: 0;
      touch-action: none;
      -ms-touch-action: none;
  }
  canvas {
      z-index: -2;
      position: absolute;
      filter: {{advanced.filter}}
  }

  {{module.header.css}}
  {{module.footer.css}}
  {{module.startScreen.css}}
  {{module.endScreen.css}}
  {{module.retryScreen.css}}

</style>

<config>
{
  "_version": 2,
  "_modules": {
   "header": {
     "id": 1
   },
   "footer": {
     "id": 1
   },
   "endScreen": {
     "id": 1
   },
   "startScreen": {
     "id": 0
   },
   "retryScreen": {
     "id": 2
   }
 },
  "general": {
    "_name": "General",
    "langs": {
      "type": "array",
      "name": "Languages",
      "value": ["en"]
    },
    "name": {
      "type": "text",
      "name": "Title",
      "value": "House moving"
    },
    "icon": {
      "type": "media",
      "name": "Icon of the game",
      "value": ""
    },
    "ios": {
      "type": "text",
      "name": "AppStore link",
      "info": "For iOS",
      "value": ""
    },
    "android": {
      "type": "text",
      "name": "PlayStore link",
      "info": "For Android",
      "value": ""
    }
  },
  "creative": {
    "_name": "Creative",

    "timerEndScreen": {
      "type": "range",
      "name": "Timer end screen",
      "value": 25,
      "min": 3,
      "max": 60
    },

    "redirectionAfterClick": {
      "type": "range",
      "name": "Redirection After X click",
      "value": 6,
      "min": 0,
      "max": 20,
      "step": 1
    },

    "nbRetry": {
      "type": "range",
      "name": "Retry number",
      "value": 0,
      "min": 0,
      "max": 10
    },

    
     "maxScore": {
      "type": "range",
      "name": "Score target",
      "value": 180,
      "min": 50,
      "max": 300,
      "step": 10
    },



      "background":{
      "type": "select",
            "name": "Background theme",
            "value": 1,
            "options": [
              { "name": "Theme1", "value": 1 },
              { "name": "Theme2", "value": 2 }
      ]
    },



    "sound":{
      "type": "select",
            "name": "Enable sound",
            "value": true,
            "options": [
              { "name": "Yes", "value": true },
              { "name": "No", "value": false }
      ]
    }

  },

  "endLevel": {
    "_name": "End Level",

    "maxScore": {
      "type": "range",
      "name": "Score target",
      "value": 250,
      "min": 50,
      "max": 300,
      "step": 10
    },

     "background":{
      "type": "select",
            "name": "Background theme",
            "value": 2,
            "options": [
              { "name": "Theme1", "value": 1 },
              { "name": "Theme2", "value": 2 }
      ]
    },



    "sound":{
      "type": "select",
            "name": "Enable sound",
            "value": true,
            "options": [
              { "name": "Yes", "value": true },
              { "name": "No", "value": false }
      ]
    },
      
    

    "available": {
      "type": "select",
      "name": "Available as End Screen",
      "value": true,
      "options": [
        { "name": "Yes", "value": true },
        { "name": "No", "value": false }
      ]
    },

    "restartAfterStore": {
      "type": "select",
      "name": "Available After Store",
      "value": true,
      "options": [
        { "name": "Yes", "value": true },
        { "name": "No", "value": false }
      ]
    },

    "delay": {
      "type": "range",
      "name": "Restart delay(in second)",
      "value": 2,
      "min": 0,
      "max": 10,
      "step":0.5
    },

    "paused": {
      "type": "select",
      "name": "Paused level",
      "value": false,
      "options": [
        { "name": "Yes", "value": true },
        { "name": "No", "value": false }
      ]
    }

  },

  "advanced": {
    "_name": "Advanced",
     "filter": {
      "type": "select",
      "name": "Filter",
      "value": "none",
      "options": [
        { "name": "No filter", "value": "none" },
        { "name": "Greyscale", "value": "grayscale(100%)" },
        { "name": "Sepia", "value": "sepia(70%)" },
        { "name": "Invert", "value": "invert(100%)" },
        { "name": "Hue 1", "value": "hue-rotate(-90deg)" },
        { "name": "Hue 2", "value": "hue-rotate(90deg)" }
      ]
    },

    "font": {
      "type": "media",
      "name": "Custom Font",
      "value": " "
    },

    "autoStart": {
      "name": "Auto Start",
			"value": false,
      "type": "select",
      "options": [
        {
          "value": true,
          "name": "True"
        },
        {
          "value": false,
          "name": "False"
        }
      ]
    },

    "autoStartDelay": {
      "type": "range",
      "name": "Auto Start Delay (in second)",
      "value": 3.5,
      "min": 0.5,
      "max": 10,
      "step":0.5
    }

  }

}
</config>
